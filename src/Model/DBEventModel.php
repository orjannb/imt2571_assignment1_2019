<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            try {
                // Connect to database
                $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4',
                            DB_USER, DB_PWD,
                            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                } catch(PDOException $ex) {
                    throw $ex;
                }
        }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
        // Retrive events from the database and add to the list, one by one
        $eventList = array();
        try {
            $stmt = $this->db->query('SELECT * FROM event ORDER BY id');
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $eventList[] = new Event($row['title'], $row['date'],
                                $row['description'], $row['id']);
            }
        } catch(PDOException $ex) {
            throw $ex;
        }
        return $eventList;
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        // Retrive the event from the database
        $event = null;
        Event::verifyId($id);

        $row = $this->db->query('SELECT * FROM event WHERE id=' .$id)->fetch(PDO::FETCH_ASSOC);

        $event = new Event($row['title'], $row['date'], $row['description'], $row['id']);
        return $event;
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
        // Add the event to the database
        try {
            $event->verify(true);

            $stmt = $this->db->prepare('INSERT INTO event (title, date, description)
                                       VALUES (:title, :date, :description)');
            $stmt->bindValue(':title', $event->title);
            $stmt->bindValue(':date', $event->date);
            $stmt->bindValue(':description', $event->description);
            $stmt->execute();

            $event->id = $this->db->LastInsertId();
        } catch(PDOException $ex) {
            throw $ex;
        }
    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
        // Modify the event in the database
        try {
            $event->verify(true);
            $stmt = $this->db->prepare('UPDATE event SET title=:title, date=:date,
                                description=:description WHERE id=' . $event->id);
        $stmt->bindValue(':title', $event->title);
        $stmt->bindValue(':date', $event->date);
        $stmt->bindValue(':description', $event->description);
        $stmt->execute();
        } catch(PDOException $ex) {
            throw $ex;
        }
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        // Delete the event from the database
        try {
            if ($id > -1) {
                $stmt = $this->db->prepare('DELETE FROM event WHERE id=' . $id);
                $stmt->execute();
            }
        } catch(PDOException $ex) {
            throw $ex;
        }
    }
}
